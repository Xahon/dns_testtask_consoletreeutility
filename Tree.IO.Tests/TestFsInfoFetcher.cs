using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tree.IO.Interfaces;

namespace Tree.IO.Tests
{
    internal class TestFsInfoFetcher : IFsInfoFetcher
    {
        public string RootNodePath { get; }
        private FsNode[] FakeFsStructure { get; }

        public Task<bool> DirectoryExists(string path)
        {
            return Task.FromResult(FakeFsStructure.Any(d => d.Kind == FsNodeKind.Directory && d.Path == path));
        }

        public Task<bool> FileExists(string path)
        {
            return Task.FromResult(FakeFsStructure.Any(f => f.Kind == FsNodeKind.File & f.Path == path));
        }

        public TestFsInfoFetcher(string rootNodePath, params FsNode[] fakeFsStructure)
        {
            RootNodePath = rootNodePath;
            FakeFsStructure = fakeFsStructure;
        }

        public Task<IEnumerable<FsNode>> GetAllDirectoriesFromRootRecursively(int depth)
        {
            var directories = FakeFsStructure.Where(n => n.Kind == FsNodeKind.Directory);
            return Task.FromResult(directories);
        }

        public Task<IEnumerable<FsNode>> GetAllFilesFromRootRecursively(int depth)
        {
            var files = FakeFsStructure.Where(n => n.Kind == FsNodeKind.File);
            return Task.FromResult(files);
        }
    }
}