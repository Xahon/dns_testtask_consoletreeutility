using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Tree.CLI
{
    public class PositionalArguments : List<string>
    {
        public PositionalArguments()
        {
        }

        public PositionalArguments(IEnumerable<string> collection) : base(collection)
        {
        }

        public PositionalArguments(int capacity) : base(capacity)
        {
        }

        public string Get(int index)
        {
            return index < 0 || index >= Count ? null : this[index];
        }

        public T? Get<T>(int index) where T : struct
        {
            if (index < 0 || index >= Count)
                return null;
            try
            {
                return (T) Convert.ChangeType(this[index], typeof(T));
            }
            catch
            {
                return null;
            }
        }
    }

    public class Arguments : List<Argument>
    {
        public Arguments()
        {
        }

        public Arguments(IEnumerable<Argument> collection) : base(collection)
        {
        }

        public Arguments(int capacity) : base(capacity)
        {
        }

        public Argument Last(params string[] aliases)
        {
            return this.LastOrDefault(argument => aliases.Contains(argument.Name, StringComparer.InvariantCultureIgnoreCase));
        }

        public bool Has(params string[] names)
        {
            return this.Any(d => names.Any(n => n.Equals(d.Name, StringComparison.InvariantCultureIgnoreCase)));
        }
    }

    public class Argument
    {
        public string Name { get; }
        public List<string> Values { get; }

        public Argument(string name, List<string> values)
        {
            Name = name;
            Values = values;
        }

        public string Get(int index = 0)
        {
            return index < 0 || index >= Values.Count ? null : Values[index];
        }

        public T? Get<T>(int index = 0) where T : struct
        {
            if (index < 0 || index >= Values.Count)
                return null;
            try
            {
                return (T) Convert.ChangeType(Values[index], typeof(T));
            }
            catch
            {
                return null;
            }
        }
    }

    public class ArgsParser
    {
        private static readonly Regex _enclosingsRegex = new Regex("(([\"'`]).*?\\2)");
        private static readonly Regex _splitPositionalArgsRegex = new Regex("\\s");
        private static readonly Regex _splitCommandArgsRegex = new Regex("(--?[^-]*)");
        private static readonly Regex _splitCommandFromValueRegex = new Regex("(--?([^\\s=]*)[\\s=]?(.*))");

        private static readonly Random _random = new Random();

        public List<CliCommandBase> RegisteredCommands { get; } = new List<CliCommandBase>();
        public PositionalArguments PositionalArguments { get; } = new PositionalArguments();
        public Arguments Arguments { get; } = new Arguments();

        public ArgsParser()
        {
        }

        public ArgsParser(string[] args)
        {
            if (args.Length <= 0)
                return;

            var argsAsSingleLine = string.Join(" ", args, 0, args.Length);
            ParseArgs(argsAsSingleLine);
        }

        public ArgsParser ParseArgs(string args)
        {
            PositionalArguments.Clear();
            Arguments.Clear();

            var argsCpy = args.Trim(' ');

            ParsePositionalArgs(ref argsCpy);
            ParseCommandArgs(ref argsCpy);

            return this;
        }

        public CliCommand RegisterCommand(string description, params string[] aliases)
        {
            var cmd = new CliCommand(description, aliases)
            {
                Arguments = Arguments
            };
            RegisteredCommands.Add(cmd);
            return cmd;
        }

        public CliCommand<T> RegisterCommand<T>(string description, T defaultValue, string helpValuePlaceholder, params string[] aliases) where T : struct
        {
            var cmd = new CliCommand<T>(description, defaultValue, helpValuePlaceholder, aliases)
            {
                Arguments = Arguments
            };
            RegisteredCommands.Add(cmd);
            return cmd;
        }

        private void ParsePositionalArgs(ref string argsString)
        {
            int firstDashIndex = argsString.IndexOf("-", StringComparison.Ordinal);
            if (firstDashIndex == 0) // -1 not found or the very first symbol is dash. It means no positional args given
                return;

            var positionals = firstDashIndex != -1 ? argsString.Substring(0, firstDashIndex) : argsString;
            var positionalsArr = _splitPositionalArgsRegex.Split(positionals).Where(s => !string.IsNullOrEmpty(s));
            PositionalArguments.AddRange(positionalsArr);
        }

        private void ParseCommandArgs(ref string argsString)
        {
            var hashedPartsMap = HashEnclosingValueParts(ref argsString);
            var commandsAndValues = SplitArgsAndValues(argsString, hashedPartsMap);

            var argsAndValueLists = commandsAndValues.GroupBy(t => t.command).ToDictionary(g => g.Key, g => g.Select(t => t.rawValue).ToList());
            foreach (var pair in argsAndValueLists)
                Arguments.Add(new Argument(pair.Key, pair.Value));
        }

        private IEnumerable<(string command, string rawValue)> SplitArgsAndValues(string argumentsString, Dictionary<string, string> hashedPartsMap)
        {
            var splitCommandsAndValues = _splitCommandArgsRegex
                .Split(argumentsString)
                .Where(s => !string.IsNullOrEmpty(s))
                .ToArray();

            var commandsAndValues = new List<(string command, string rawValue)>();

            for (int i = 0; i < splitCommandsAndValues.Length; i++)
            {
                var commandValuePairString = splitCommandsAndValues[i];
                var groups = _splitCommandFromValueRegex.Match(commandValuePairString).Groups;

                string sanitizedCommand = groups[2].Value.Trim(' ').TrimStart('-');
                string sanitizedValue = groups[3].Value.Trim(' ', '"').TrimStart('-');

                if (!string.IsNullOrEmpty(sanitizedValue))
                {
                    // Unhash previously hashed quoted strings
                    foreach (var hashedPart in hashedPartsMap)
                        sanitizedValue = sanitizedValue.Replace(hashedPart.Key, hashedPart.Value);

                    // Fix repeating of double quotes in "\"value\""
                    if (sanitizedValue.StartsWith("\"") && sanitizedValue.EndsWith("\"") && sanitizedValue.Length > 3)
                    {
                        sanitizedValue = sanitizedValue.Substring(1, sanitizedValue.Length - 2);
                    }
                }

                commandsAndValues.Add((sanitizedCommand, sanitizedValue));
            }

            return commandsAndValues;
        }

        private Dictionary<string, string> HashEnclosingValueParts(ref string argumentsString)
        {
            var hashedPartsAndOrigs = new Dictionary<string, string>();
            while (_enclosingsRegex.IsMatch(argumentsString))
            {
                string replacementKey;
                do
                {
                    replacementKey = GenerateRandomString();
                } while (argumentsString.Contains(replacementKey) || hashedPartsAndOrigs.ContainsKey(replacementKey));

                var match = _enclosingsRegex.Match(argumentsString);
                var origValue = match.Captures[0].Value;

                argumentsString = argumentsString.Replace(origValue, replacementKey);
                hashedPartsAndOrigs.Add(replacementKey, origValue);
            }

            return hashedPartsAndOrigs;
        }

        private string GenerateRandomString(int length = 10)
        {
            const string set = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(set, length).Select(s => s[_random.Next(set.Length)]).ToArray());
        }
    }
}