using System.Linq;

namespace Tree.CLI
{
    public class CliCommandBase
    {
        public string Description { get; }
        public string[] Aliases { get; }
        internal Arguments Arguments { get; set; }

        internal CliCommandBase(string description, params string[] aliases)
        {
            Description = description;
            Aliases = aliases;
        }

        public bool IsProvided() => Arguments.Has(GetTrimmedAliases());
        protected string[] GetTrimmedAliases() => Aliases.Select(a => a.TrimStart('-')).ToArray();
        public string GetJoinedAliasesString() => string.Join(", ", Aliases);
    }

    public class CliCommand : CliCommandBase
    {
        internal CliCommand(string description, params string[] aliases) : base(description, aliases)
        {
        }

        public bool Get() => Arguments.Has(GetTrimmedAliases());
    }

    public class CliCommand<T> : CliCommandBase where T : struct
    {
        private T DefaultValue { get; }
        public string HelpValuePlaceholder { get; }

        internal CliCommand(string description, T defaultValue, string helpValuePlaceholder, params string[] aliases) : base(description, aliases)
        {
            DefaultValue = defaultValue;
            HelpValuePlaceholder = helpValuePlaceholder;
        }

        public T GetValue()
        {
            return Arguments.Last(GetTrimmedAliases())?.Get<T>() ??
                   DefaultValue;
        }
    }
}