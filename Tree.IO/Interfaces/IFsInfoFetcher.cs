using System.Collections.Generic;
using System.Threading.Tasks;

namespace Tree.IO.Interfaces
{
    public interface IFsInfoFetcher
    {
        string RootNodePath { get; }

        Task<bool> DirectoryExists(string path);
        Task<bool> FileExists(string path);

        Task<IEnumerable<FsNode>> GetAllDirectoriesFromRootRecursively(int depth);
        Task<IEnumerable<FsNode>> GetAllFilesFromRootRecursively(int depth);
    }
}