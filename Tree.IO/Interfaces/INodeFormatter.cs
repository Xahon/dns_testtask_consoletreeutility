namespace Tree.IO.Interfaces
{
    public interface INodeFormatter
    {
        string GetInfoString(FsNode node);
    }
}