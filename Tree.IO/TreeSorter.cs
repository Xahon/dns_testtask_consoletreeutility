using System;
using System.Collections.Generic;
using System.Linq;

namespace Tree.IO
{
    public class TreeSorter
    {
        public SortRule SortRule { get; }
        public bool DirectoriesAtTheTop { get; }

        public TreeSorter(SortRule sortRule, bool directoriesAtTheTop = false)
        {
            SortRule = sortRule;
            DirectoriesAtTheTop = directoriesAtTheTop;
        }

        public IEnumerable<FsNode> Sort(IEnumerable<FsNode> nodes)
        {
            if (DirectoriesAtTheTop)
            {
                return nodes
                    .GroupBy(n => n.Kind)
                    .OrderBy(g => g.Key)
                    .SelectMany(g => GetOrderedNodes(g.AsEnumerable()));
            }

            return GetOrderedNodes(nodes);
        }

        private IEnumerable<FsNode> GetOrderedNodes(IEnumerable<FsNode> fsNodes)
        {
            switch (SortRule.SortDirection)
            {
                case SortDirection.Asc:
                    return fsNodes.OrderBy(GetSortKey);
                case SortDirection.Desc:
                    return fsNodes.OrderByDescending(GetSortKey);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private IComparable GetSortKey(FsNode node)
        {
            switch (SortRule.SortKey)
            {
                case SortKey.Alphabetical:
                    return node.Name;
                case SortKey.Size:
                    return node.Size;
                case SortKey.ModifiedDate:
                    return node.ModifiedDate;
                case SortKey.CreatedDate:
                    return node.CreatedDate;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    public class SortRule
    {
        public SortKey SortKey { get; }
        public SortDirection SortDirection { get; }

        public SortRule(SortKey sortKey, SortDirection sortDirection)
        {
            SortKey = sortKey;
            SortDirection = sortDirection;
        }
    }

    public enum SortDirection
    {
        Asc,
        Desc
    }

    public enum SortKey
    {
        Alphabetical,
        Size,
        ModifiedDate,
        CreatedDate,
    }
}