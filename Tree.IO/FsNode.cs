using System;
using System.Collections.Generic;

namespace Tree.IO
{
    public enum FsNodeKind
    {
        Directory = 0,
        File
    }

    public class FsNode : IEquatable<FsNode>
    {
        public string Path { get; }
        public FsNodeKind Kind { get; }
        public long Size { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public FsNode Parent { get; set; }
        public List<FsNode> Children { get; } = new List<FsNode>();

        public FsNode(string path, FsNodeKind kind)
        {
            Kind = kind;
            Path = PathSanitizer.SanitizePath(path);
        }

        public bool IsRoot(string rootNodePath) => rootNodePath == Path;

        private string _name;
        public string Name => _name ?? (_name = System.IO.Path.GetFileName(Path));

        private string _parentPath;
        public string ParentPath
        {
            get
            {
                if (_parentPath != null)
                    return _parentPath;

                var indexOfName = Path.IndexOf(Name, StringComparison.Ordinal);
                if (indexOfName == -1)
                    return _parentPath = null;

                return _parentPath = Path.Substring(0, indexOfName).TrimEnd(System.IO.Path.DirectorySeparatorChar);
            }
        }

        public int Depth
        {
            get
            {
                if (Parent == null)
                    return 0;
                return Parent.Depth + 1;
            }
        }

        public override string ToString()
        {
            return $"{Kind.ToString()}: {{{Path}}} ({Size} Bytes)";
        }

        public bool Equals(FsNode other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Path, other.Path);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((FsNode) obj);
        }

        public override int GetHashCode()
        {
            return Path != null ? Path.GetHashCode() : 0;
        }
    }
}