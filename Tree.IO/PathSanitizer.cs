namespace Tree.IO
{
    public static class PathSanitizer
    {
        public static string SanitizePath(string path) => path.TrimEnd('/');
    }
}