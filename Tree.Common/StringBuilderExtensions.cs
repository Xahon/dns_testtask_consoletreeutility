using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tree.Common
{
    public static class StringBuilderExtensions
    {
        public static int WriteAt(this StringBuilder sb, int position, params char[] chars)
        {
            sb.EnsureCapacity(position + chars.Length);
            int oldLen = sb.Length;
            sb.Length = sb.Capacity;
            for (int i = oldLen; i < sb.Capacity; i++)
                sb[i] = ' ';

            for (int i = 0; i < chars.Length; i++)
                sb[position + i] = chars[i];

            return position + chars.Length;
        }

        public static int WriteAt(this StringBuilder sb, int position, string text)
        {
            return WriteAt(sb, position, text.ToCharArray());
        }

        public static int WriteAt(this StringBuilder sb, int position, IEnumerable<char> text)
        {
            return WriteAt(sb, position, text.ToArray());
        }
    }
}