using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Tree.IO.Interfaces;

namespace Tree.IO
{
    public class LocalFsInfoFetcher : IFsInfoFetcher
    {
        public string RootNodePath { get; }

        public LocalFsInfoFetcher(string rootPath)
        {
            RootNodePath = Path.GetFullPath(rootPath);
        }

        public Task<bool> DirectoryExists(string path)
        {
            return Task.FromResult(new DirectoryInfo(path).Exists);
        }

        public Task<bool> FileExists(string path)
        {
            return Task.FromResult(new FileInfo(path).Exists);
        }

        public Task<IEnumerable<FsNode>> GetAllDirectoriesFromRootRecursively(int depth)
        {
            return Task.FromResult(
                GetDirectories(RootNodePath, depth)
                    .Select(CreateFsNode)
            );
        }

        public Task<IEnumerable<FsNode>> GetAllFilesFromRootRecursively(int depth)
        {
            return Task.FromResult(
                GetDirectories(RootNodePath, depth - 1)
                    .SelectMany(d => d.EnumerateFiles())
                    .Select(CreateFsNode)
            );
        }

        private FsNode CreateFsNode(FileSystemInfo fsInfo)
        {
            var isDirectory = fsInfo is DirectoryInfo;
            var asDirectory = fsInfo as DirectoryInfo;
            var asFile = fsInfo as FileInfo;

            var fsNode = new FsNode(fsInfo.FullName, isDirectory ? FsNodeKind.Directory : FsNodeKind.File)
            {
                Size = isDirectory ? asDirectory.EnumerateFiles("*", SearchOption.AllDirectories).Sum(f => f.Length) : asFile.Length,
                CreatedDate = fsInfo.CreationTime,
                ModifiedDate = fsInfo.LastWriteTime
            };


            return fsNode;
        }

        private IEnumerable<DirectoryInfo> GetDirectories(string root, int depth = -1)
        {
            return GetDirectories(new DirectoryInfo(root), depth);
        }

        private IEnumerable<DirectoryInfo> GetDirectories(DirectoryInfo root, int depth = -1)
        {
            yield return root;
            if (depth >= 0)
                foreach (var dir in root.EnumerateDirectories())
                foreach (var subdir in GetDirectories(dir, depth - 1))
                    yield return subdir;
        }
    }
}