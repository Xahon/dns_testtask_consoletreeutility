using System.Collections.Generic;
using NUnit.Framework;

namespace Tree.IO.Tests
{
    [TestFixture]
    internal class TreeSorterTest
    {
        [Test]
        public void SortsByNameAsc()
        {
            var fsNodes = MockGenerator.GetTestFileStructure();
            var sorter = new TreeSorter(new SortRule(SortKey.Alphabetical, SortDirection.Asc));
            var orderedFsNodesQueue = new Queue<FsNode>(sorter.Sort(fsNodes));

            Assert.AreEqual("$weird_file$", orderedFsNodesQueue.Dequeue().Name);
            Assert.AreEqual(".gitignore", orderedFsNodesQueue.Dequeue().Name);
            Assert.AreEqual("__underscored__", orderedFsNodesQueue.Dequeue().Name);
            Assert.AreEqual("123", orderedFsNodesQueue.Dequeue().Name);
            Assert.AreEqual("987", orderedFsNodesQueue.Dequeue().Name);
            Assert.AreEqual("aaa", orderedFsNodesQueue.Dequeue().Name);
            Assert.AreEqual("aab", orderedFsNodesQueue.Dequeue().Name);
            Assert.AreEqual("aba", orderedFsNodesQueue.Dequeue().Name);
            Assert.AreEqual("index.html", orderedFsNodesQueue.Dequeue().Name);
            Assert.AreEqual("zzz", orderedFsNodesQueue.Dequeue().Name);
        }
    }
}