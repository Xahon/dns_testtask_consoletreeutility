using System;

namespace Tree.IO.Interfaces
{
    public class CallbackNodeFormatter : INodeFormatter
    {
        private Func<FsNode, string> FormatInfoDelegate { get; }

        public CallbackNodeFormatter(Func<FsNode, string> formatInfoDelegate)
        {
            FormatInfoDelegate = formatInfoDelegate;
        }

        public string GetInfoString(FsNode node)
        {
            return FormatInfoDelegate.Invoke(node);
        }
    }
}