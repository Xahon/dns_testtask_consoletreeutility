using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Tree.Common;
using Tree.IO.Interfaces;

namespace Tree.IO
{
    public class TreeBuilder
    {
        public IFsInfoFetcher InfoFetcher { get; }
        public FsNode RootNode { get; private set; }

        public TreeBuilder(IFsInfoFetcher infoFetcher)
        {
            InfoFetcher = infoFetcher;
        }

        public async Task BuildTree(int depth)
        {
            var dirs = (await InfoFetcher.GetAllDirectoriesFromRootRecursively(depth)).ToList();
            var files = (await InfoFetcher.GetAllFilesFromRootRecursively(depth)).ToList();
            var rootNodePath = PathSanitizer.SanitizePath(InfoFetcher.RootNodePath);

            RootNode = new FsNode(rootNodePath, FsNodeKind.Directory);

            FsNode CreateParentNodeForOrphan(FsNode orphan)
            {
                if (orphan.IsRoot(rootNodePath)) // Root directory is always orphaned
                    return null;

                var parentNode = new FsNode(orphan.ParentPath, FsNodeKind.Directory);
                return parentNode;
            }

            var pathNodeMap = new Dictionary<string, FsNode>(dirs.Count + files.Count + 1);

            var nodesQueue = new Queue<FsNode>(
                EnumerableExtensions.Concat(new[] {RootNode}, dirs, files)
            );

            while (nodesQueue.Any())
            {
                FsNode currentNode = nodesQueue.Dequeue();
                string currentPath = PathSanitizer.SanitizePath(currentNode.Path);

                if (!pathNodeMap.ContainsKey(currentPath))
                    pathNodeMap.Add(currentPath, currentNode);

                if (currentNode.Parent != null || currentNode.IsRoot(rootNodePath))
                    continue;

                if (!pathNodeMap.TryGetValue(currentNode.ParentPath, out var parentNode))
                {
                    parentNode = CreateParentNodeForOrphan(currentNode);
                    string parentPath = PathSanitizer.SanitizePath(parentNode.Path);

                    pathNodeMap.Add(parentPath, parentNode);
                }

                if (parentNode == null)
                    continue;

                nodesQueue.Enqueue(parentNode);

                currentNode.Parent = parentNode;
                parentNode.Children.Add(currentNode);
            }
        }

        public class TreeIteratorEntry
        {
            public FsNode Node { get; }
            public Queue<FsNode> ChildQueue { get; }

            public TreeIteratorEntry(FsNode node, IEnumerable<FsNode> childQueue)
            {
                Node = node;
                ChildQueue = new Queue<FsNode>(childQueue);
            }
        }

        public delegate void TreeIteratorDelegate(FsNode currentNode, ReadOnlyCollection<TreeIteratorEntry> dirsStack, TreeIteratorEntry currentEntry);

        public void IterateTree(TreeSorter sorter, TreeIteratorDelegate iteratorDelegate)
        {
            var dirsStack = new Stack<TreeIteratorEntry>();

            void PushDir(FsNode dir)
            {
                dirsStack.Push(new TreeIteratorEntry(dir, sorter.Sort(dir.Children)));
            }

            PushDir(RootNode);

            while (dirsStack.Any())
            {
                var entry = dirsStack.Peek();

                if (!entry.ChildQueue.Any())
                {
                    dirsStack.Pop();
                    continue;
                }

                var child = entry.ChildQueue.Dequeue();
                iteratorDelegate.Invoke(child, dirsStack.ToList().AsReadOnly(), entry);

                if (child.Kind == FsNodeKind.Directory)
                    PushDir(child);
            }
        }
    }
}