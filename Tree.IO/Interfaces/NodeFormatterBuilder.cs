using System;

namespace Tree.IO.Interfaces
{
    public class NodeFormatterBuilder : INodeFormatter
    {
        private Func<FsNode, string> _sizeConverter;
        private Func<FsNode, string> _nameConverter;

        public NodeFormatterBuilder()
        {
            SetNameConverter(node => node.Name);
            SetSizeConverter(node => node.Size.ToString());
        }

        public string GetInfoString(FsNode node)
        {
            return $"{_nameConverter(node)} {_sizeConverter(node)}";
        }

        public NodeFormatterBuilder SetNameConverter(Func<FsNode, string> converter)
        {
            _nameConverter = converter;
            return this;
        }

        public NodeFormatterBuilder SetNameConverterFilesOnly()
        {
            _nameConverter = node => node.Kind == FsNodeKind.File ? _sizeConverter.Invoke(node) : "";
            return this;
        }

        public NodeFormatterBuilder SetSizeConverter(Func<FsNode, string> converter)
        {
            _sizeConverter = converter;
            return this;
        }
    }
}