﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tree.CLI;
using Tree.IO;
using Tree.IO.Interfaces;

namespace Tree.App
{
    internal static class Program
    {
        private static ArgsParser _argsParser;

        public static int Main(string[] args)
        {
            var task = Run(args);
            task.Wait();
            return task.Result;
        }

        private static async Task<int> Run(string[] args)
        {
            _argsParser = new ArgsParser(args);

            // @formatter:off
            CliCommand dirSizeCommand = _argsParser.RegisterCommand(
                "Show size on directory entries",
                "-z", "--dir-size"
            );

            CliCommand humanReadableCommand = _argsParser.RegisterCommand(
                "Provide human-readable sizes",
                "-r", "--human-readable"
            );

            CliCommand<int> depthCommand = _argsParser.RegisterCommand(
                "Limit max files depth (Warning: dirsize causes all files to enumerate, despite this option)",
                int.MaxValue,
                "count",
                "-d", "--depth"
            );

            CliCommand<char> sortCommand = _argsParser.RegisterCommand(
                "Specifies sort type (Acceptable values are 'a' - alphabetic, 's' - size, 'c' - created date, 'm' - modified date)",
                'a',
                "order",
                "-s", "--sort"
            );

            CliCommand sortDescending = _argsParser.RegisterCommand(
                "Specifies descending sort order (Default ascending)",
                "-c","--desc"
            );

            CliCommand dirsAtTop = _argsParser.RegisterCommand(
                "Place directories at the top",
                "-t, --dirs-top"
            );

            CliCommand helpCommand = _argsParser.RegisterCommand(
                "Shows this message",
                "-h", "--help"
            );
            // @formatter:on

            if (helpCommand.IsProvided())
            {
                ShowHelp();
                return 0;
            }

            string rootPath = _argsParser.PositionalArguments.ElementAtOrDefault(0);

            if (rootPath == null)
            {
#if DEBUG
                rootPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
#else
                Console.Error.WriteLine("Error: No path provided");
                return 1;
#endif
            }

            var fsInfoFetcher = new LocalFsInfoFetcher(rootPath);
            if (!await fsInfoFetcher.DirectoryExists(rootPath))
            {
                Console.Error.WriteLine($"Directory {rootPath} doesn't exist");
                return 2;
            }


            var treeBuilder = new TreeBuilder(fsInfoFetcher);
            await treeBuilder.BuildTree(depthCommand.GetValue());

            var nodeFormatter = new NodeFormatterBuilder();
            if (humanReadableCommand.IsProvided())
            {
                bool showSizeOnDirectories = dirSizeCommand.IsProvided();

                nodeFormatter.SetSizeConverter(node =>
                {
                    if (node.Kind == FsNodeKind.Directory && !showSizeOnDirectories)
                        return "";

                    if (node.Size == 0)
                        return "(empty)";
                    return $"({SizeFormatter.ToBytesCount(node.Size)})";
                });
            }

            var treeSorter = new TreeSorter(new SortRule(GetSortOrder(sortCommand), GetSortDirection(sortDescending.IsProvided())), dirsAtTop.IsProvided());
            var treeFormatter = new TreeFormatter(nodeFormatter);
            var writer = new StreamWriter(Console.OpenStandardOutput())
            {
                AutoFlush = true
            };
            Console.OutputEncoding = Encoding.UTF8;
            Console.SetOut(writer);
            Console.ForegroundColor = ConsoleColor.White;

            writer.WriteLine(rootPath);
            treeFormatter.FormatOutput(treeBuilder, treeSorter, writer);
            return 0;
        }

        private static SortKey GetSortOrder(CliCommand<char> sortCommand)
        {
            switch (sortCommand.GetValue())
            {
                case 'a': return SortKey.Alphabetical;
                case 's': return SortKey.Size;
                case 'c': return SortKey.CreatedDate;
                case 'm': return SortKey.ModifiedDate;
                default: throw new Exception($"Unknown option '{sortCommand.GetValue()}' in command sort");
            }
        }

        private static SortDirection GetSortDirection(bool desc)
        {
            return desc ? SortDirection.Desc : SortDirection.Asc;
        }

        private static void ShowHelp()
        {
            var sb = new StringBuilder();
            int commandIndent = _argsParser.RegisteredCommands.Max(c => c.GetJoinedAliasesString().Length) + 20;
            const int descriptionIndent = 90;
            string format = $"{{0,{-commandIndent}}}{{1,{-descriptionIndent}}}";

            sb.AppendLine("usage: tree <PATH>").AppendLine();
            _argsParser.RegisteredCommands.ForEach(c =>
            {
                string commandString = c.GetJoinedAliasesString();

                var t = c.GetType();
                if (t.IsGenericType)
                {
                    string helpPlaceholder = (string) typeof(CliCommand<>).MakeGenericType(t.GetGenericArguments()).GetProperty("HelpValuePlaceholder")
                        ?.GetValue(c);
                    commandString += $" <{helpPlaceholder}>";
                }

                sb.AppendFormat(format, commandString, c.Description).AppendLine();
            });

            Console.WriteLine(sb);
        }
    }
}