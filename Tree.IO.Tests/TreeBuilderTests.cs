using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Tree.IO.Tests
{
    [TestFixture]
    internal class TreeBuilderTests
    {
        [Test]
        public async Task BuildsValidTree()
        {
            var infoRetriever = new TestFsInfoFetcher(
                "testDir",
                new FsNode("testDir/index.html", FsNodeKind.File)
                {
                    Size = 10
                },
                new FsNode("testDir/js/index.js", FsNodeKind.File)
                {
                    Size = 100
                },
                new FsNode("testDir/css/style.css", FsNodeKind.File)
                {
                    Size = 1000
                },
                new FsNode("testDir/css/empty_dir", FsNodeKind.Directory)
            );

            var treeBuilder = new TreeBuilder(infoRetriever);
            await treeBuilder.BuildTree(100);

            Assert.AreEqual("testDir", treeBuilder.RootNode.Name);
            Assert.AreEqual(3, treeBuilder.RootNode.Children.Count);
            Assert.That(() => treeBuilder.RootNode.Children.First(n => n.Name == "js").Children.Any(n => n.Name == "index.js"));
        }
    }
}