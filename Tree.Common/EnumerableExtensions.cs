using System;
using System.Collections.Generic;
using System.Linq;

namespace Tree.Common
{
    public static class EnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (var e in enumerable)
                action.Invoke(e);
        }

        public static IEnumerable<T> Concat<T>(params IEnumerable<T>[] enumerables)
        {
            return enumerables.SelectMany(e => e);
        }
    }
}