using System;
using System.IO;
using System.Linq;
using System.Text;
using Tree.Common;
using Tree.IO.Formatters;
using Tree.IO.Interfaces;

namespace Tree.IO
{
    public class TreeFormatter
    {
        public class ConnectorSettings
        {
            public char Pipe = '┃';
            public char TopDownConnector = '┣';
            public char TopRightConnector = '┗';
            public string NameConnector = "━━ ";
        }

        public ConnectorSettings Settings { get; }
        public INodeFormatter NodeFormatter { get; }

        public TreeFormatter(ConnectorSettings settings, INodeFormatter nodeFormatter)
        {
            Settings = settings;
            NodeFormatter = nodeFormatter;
        }

        public TreeFormatter(INodeFormatter nodeFormatter) : this(new ConnectorSettings(), nodeFormatter)
        {
        }

        public TreeFormatter() : this(new SimpleNodeFormatter())
        {
        }

        public void FormatOutput(TreeBuilder treeBuilder, TreeSorter sorter, StreamWriter writer)
        {
            treeBuilder.IterateTree(sorter, (currentNode, dirsStack, parentEntry) =>
            {
                string nodeInfoString = NodeFormatter.GetInfoString(currentNode);
                int connectorPos = GetNodeConnectorStartPosition(currentNode);
                int nodePos = GetNodeNameStartPosition(currentNode);
                int maxStrLength = nodePos + nodeInfoString.Length;

                var sb = new StringBuilder("".PadLeft(maxStrLength, ' '));

                // Write ongoing vertical guides
                dirsStack
                    .Where(entry => entry.ChildQueue.Any())
                    .ForEach(d => { sb.WriteAt(GetNodeNameStartPosition(d.Node), Settings.Pipe); });

                // Write vertical connector
                sb.WriteAt(connectorPos, parentEntry.ChildQueue.Any() ? Settings.TopDownConnector : Settings.TopRightConnector);

                // Write horizontal connector
                sb.WriteAt(connectorPos + 1, Settings.NameConnector);

                // Write node
                sb.WriteAt(nodePos, nodeInfoString);
                sb.Append(Environment.NewLine);

                writer.Write(sb);
            });
        }

        private int GetNodeNameStartPosition(FsNode node) => GetNodeConnectorStartPosition(node) + Settings.NameConnector.Length + 1;

        private int GetNodeConnectorStartPosition(FsNode node) =>
            (Settings.NameConnector.Length + 1) * node.Depth - (Settings.NameConnector.Length + 1);
    }
}