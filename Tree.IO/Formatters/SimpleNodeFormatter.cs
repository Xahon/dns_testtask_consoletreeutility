using Tree.IO.Interfaces;

namespace Tree.IO.Formatters
{
    public class SimpleNodeFormatter : INodeFormatter
    {
        public string GetInfoString(FsNode node)
        {
            return node.Name;
        }
    }
}