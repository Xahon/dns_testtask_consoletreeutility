﻿using NUnit.Framework;

namespace Tree.CLI.Tests
{
    [TestFixture]
    internal class ArgsParserTests
    {
        [Test]
        public void ParsingWithoutArgsDoesntThrow()
        {
            ArgsParser CreateArgParser() => new ArgsParser().ParseArgs("");
            ArgsParser argParser = null;

            Assert.DoesNotThrow(() => argParser = CreateArgParser());

            Assert.DoesNotThrow(() => argParser.PositionalArguments.Get(0));
            Assert.DoesNotThrow(() => argParser.Arguments.Last("name")?.Get());
        }

        [Test]
        public void ParsingWithOnlyPositionalArgs()
        {
            var argParser = new ArgsParser().ParseArgs("a b c d");

            Assert.AreEqual("a", argParser.PositionalArguments.Get(0));
            Assert.AreEqual("b", argParser.PositionalArguments.Get(1));
            Assert.AreEqual("c", argParser.PositionalArguments.Get(2));
            Assert.AreEqual("d", argParser.PositionalArguments.Get(3));
        }

        [Test]
        public void ParsingWithMultipleTypesOfArguments()
        {
            const string testStringArgumentValue = "test arg in quotes";
            const string testStringArgumentValue2 = "another one";
            const int testIntArgumentValue = 200;
            string mockArgs = $"positionalArgument1 positionalArgument2 12345 --debug -m -n --str=\"{testStringArgumentValue}\" " +
                              $"--str2 \"{testStringArgumentValue2}\" " +
                              $"--count {testIntArgumentValue} --key key0 --key key1 --key key2";

            var argParser = new ArgsParser();
            argParser.ParseArgs(mockArgs);

            Assert.NotNull(argParser.PositionalArguments.Get(0));
            Assert.NotNull(argParser.PositionalArguments.Get(1));
            Assert.NotNull(argParser.PositionalArguments.Get(2));
            Assert.Null(argParser.PositionalArguments.Get(3));

            Assert.AreEqual("positionalArgument" + 1, argParser.PositionalArguments.Get(0));
            Assert.AreEqual("positionalArgument" + 2, argParser.PositionalArguments.Get(1));
            Assert.AreEqual(12345, argParser.PositionalArguments.Get<int>(2));

            Assert.True(argParser.Arguments.Has("debug"));
            Assert.True(argParser.Arguments.Has("m"));
            Assert.True(argParser.Arguments.Has("n"));
            Assert.False(argParser.Arguments.Has("notExists"));

            Assert.AreEqual(testStringArgumentValue, argParser.Arguments.Last("str").Get());
            Assert.AreEqual(testStringArgumentValue2, argParser.Arguments.Last("str2").Get());
            Assert.AreEqual(testIntArgumentValue, argParser.Arguments.Last("count").Get<int>());

            Assert.AreEqual("key0", argParser.Arguments.Last("key").Get(0));
            Assert.AreEqual("key1", argParser.Arguments.Last("key").Get(1));
            Assert.AreEqual("key2", argParser.Arguments.Last("key").Get(2));
            Assert.IsNull(argParser.Arguments.Last("key").Get(3));
        }

/*        [Test]
        public void ParsingCombinedArguments()
        {
            var argsParser = new ArgsParser();
            argsParser.ParseArgs("-wsad");

            Assert.NotNull(argsParser.GetFirst("w"));
            Assert.NotNull(argsParser.GetFirst("s"));
            Assert.NotNull(argsParser.GetFirst("a"));
            Assert.NotNull(argsParser.GetFirst("d"));
        }*/
    }
}