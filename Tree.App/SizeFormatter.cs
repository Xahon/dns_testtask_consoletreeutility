using System;

namespace Tree.App
{
    public static class SizeFormatter
    {
        // Taken from: https://stackoverflow.com/a/49535675/6119618
        public static string ToBytesCount(long bytes, bool isIso = true)
        {
            int unit = 1024;
            string unitStr = "b";
            if (!isIso)
                unit = 1000;

            if (bytes < unit)
                return $"{bytes} {unitStr}";

            unitStr = unitStr.ToUpper();
            if (isIso)
                unitStr = "i" + unitStr;

            int exp = (int) (Math.Log(bytes) / Math.Log(unit));
            return $"{bytes / Math.Pow(unit, exp):##.##} {"KMGTPEZY"[exp - 1]}{unitStr}";
        }
    }
}