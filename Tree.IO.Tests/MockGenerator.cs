using System.Collections.Generic;

namespace Tree.IO.Tests
{
    internal static class MockGenerator
    {
        internal static IEnumerable<FsNode> GetTestFileStructure() => new[]
        {
            new FsNode("index.html", FsNodeKind.File),
            new FsNode(".gitignore", FsNodeKind.File),
            new FsNode("__underscored__", FsNodeKind.File),
            new FsNode("$weird_file$", FsNodeKind.File),
            new FsNode("aba", FsNodeKind.File),
            new FsNode("zzz", FsNodeKind.File),
            new FsNode("aaa", FsNodeKind.File),
            new FsNode("123", FsNodeKind.File),
            new FsNode("aab", FsNodeKind.File),
            new FsNode("987", FsNodeKind.File),
        };
    }
}